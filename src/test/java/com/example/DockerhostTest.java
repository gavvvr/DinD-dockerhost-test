package com.example;

import fi.iki.elonen.NanoHTTPD;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.images.builder.ImageFromDockerfile;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.ClassLoader.getSystemResource;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

public class DockerhostTest {

    public static final String REFERENCE_HTTP_BODY = Integer.toString(42);
    public static final int TEST_ENDPOINT_PORT = 10001;
    @ClassRule
    public static GenericContainer container = new GenericContainer(new ImageFromDockerfile()
            .withFileFromPath("dockerhost.sh", getResource("docker/dockerhost.sh"))
            .withFileFromPath("Dockerfile", getResource("docker/Dockerfile")))
            ;

    @Test
    public void dockerhostTest() throws Exception {
        new NanoHTTPD(TEST_ENDPOINT_PORT){
            @Override
            public Response serve(IHTTPSession session) {
                return newFixedLengthResponse(REFERENCE_HTTP_BODY);
            }
        }.start();

        System.out.println(container.execInContainer("ps").getStdout());
        System.out.println("/etc/hosts content:");
        System.out.println(container.execInContainer("cat", "/etc/hosts").getStdout());
        System.out.println("Resolving dockerhost with nslookup:");
        System.out.println(container.execInContainer("nslookup", "dockerhost").getStdout());

        Container.ExecResult execCurl = container.execInContainer("curl", "-s", "dockerhost:" + TEST_ENDPOINT_PORT);
        if (execCurl.getStdout() != null && !execCurl.getStdout().equals("")) {
            System.err.println("curl exited with non-zero code!");
            fail(execCurl.getStdout());
        }
        String responseBody = execCurl.getStdout();
        assertThat(responseBody, is(equalTo(REFERENCE_HTTP_BODY)));
    }

    private static Path getResource(String path) {
        try {
            return Paths.get(getSystemResource(path).toURI());
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Can't load resource file", e);
        }
    }

}
