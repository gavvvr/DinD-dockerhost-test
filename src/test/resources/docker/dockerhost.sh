#!/bin/sh

readonly MAC_DOCKERHOST_NAME=docker.for.mac.localhost

try_get_mac_dockerhost_ip () {
    if [ -x "$(command -v getent)" ]; then
        mac_dockerhost_ip=$(getent ahostsv4 ${MAC_DOCKERHOST_NAME} | awk 'NR==1{print $1}')
    elif [ -x "$(command -v nslookup)" ]; then
        mac_dockerhost_ip=$(nslookup ${MAC_DOCKERHOST_NAME} | grep Address | awk -F":" 'NR==1{print $2}')
    fi
}

try_get_bridged_dockerhost_ip(){
    bridged_dockerhost_ip=$(hostname -i | cut -d"." -f1-2).0.1
}

try_get_mac_dockerhost_ip
try_get_bridged_dockerhost_ip

dockerhost_ip=${mac_dockerhost_ip:-${bridged_dockerhost_ip}}

if [ -n "$dockerhost_ip" ]; then
    grep dockerhost /etc/hosts || echo ${dockerhost_ip} dockerhost >> /etc/hosts
fi

exec /bin/watch ps
